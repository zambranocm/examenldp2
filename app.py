from config import config
from flask import Flask, render_template, request, redirect, url_for, flash
from werkzeug.security import generate_password_hash
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///example.db'
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    lastname = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    age = db.Column(db.Integer)
    tipo = db.Column(db.String(50))

    def __repr__(self):
        return '<User %r>' % self.name



@app.route('/')
def index():
    return render_template('/index.html')

@app.route('/', methods=['POST'])
def reg():
    tipo = request.form.get('prof')
    name = request.form.get('name')
    apellido = request.form('lastname')
    edad = request.form('age')
    email = request.form('email')
    password = request.form.get('password')

    user = User.query.filter_by(email=email).first()

    if user: # if a user is found, we want to redirect back to signup page so user can try again
        flash('Email address already exists')
        return redirect(url_for('auth.signup'))

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(email=email, name=name, password=generate_password_hash(password, method='sha256'), tipo = tipo, lastname=apellido, age=edad)

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    render_template('/index.html')

if(__name__=='__main__'):
    app.config.from_object(config['development'])
    with app.app_context():
        db.create_all()
    app.run()
